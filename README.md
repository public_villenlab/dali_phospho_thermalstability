# Using Dali to analyze the effect of phosphorylation on thermostability

This repository is designated for the relevant code and analysis of the Smith _et al._ (BioRxiv 2020) dataset using a new proteomic method Dali, which identifies phosphorylation events that alter protein thermal stability. Also, the re-analysis of the Huang _et al._ (Nat. Methods 2019) dataset using a method called Hotspot Thermal Profiling (HTP) is included in the commentary for comparison to the Dali method's reproducibility and data reliablity. 

To date manuscript reference for repository (as of 3/29/2021):

Ian R. Smith, Kyle N. Hess, Anna A. Bakhtina, Anthony S. Valente, Ricard A. Rodríguez-Mias & Judit Villén. Identification of phosphosites that alter protein thermal stability. _BioRxiv._ (**2020**). DOI: [https://doi.org/10.1101/2020.01.14.904300](https://www.biorxiv.org/content/10.1101/2020.01.14.904300v1)

Reference to HTP method initial publication used for re-analysis:

Jun X. Huang, Gihoon Lee, Kate E. Cavanaugh, Jae W. Chang, Margaret L. Gardel & Raymond E. Moellering. High throughput discovery of functional protein modifications by Hotspot Thermal Profiling. _Nature Methods._ 16, 894–901 (**2019**). DOI: [https://doi.org/10.1038/s41592-019-0499-3](https://www.nature.com/articles/s41592-019-0499-3)

## All analysis compiled in Rmd-knitted html

**[all_dali_data_analysis](https://dali-phospho-thermostability.netlify.app/)**

All data analysis and figures for the manuscript can be viewed from above html document that was knitted from Rmarkdown located in this repository. With this link, one can explore the manuscript's data analysis with interwoven commentary. All code can be accessed via "Code" tabs in html document. 

The html document splits the analysis into 4 sections: 


1. HTP Supplement Re-analysis : from Huang _et al._ (Nat. Methods 2019) - to assess HTP reproducibility
2. Re-analysis of RAW data from Huang _et al._ (Nat. Methods 2019) - to generate phosphoisoform and unmodified peptide melting curves from phosphorylation-enriched sample
3. Dali - all relevant analysis from Dali dataset in _S. cerevisiae_
4. Moellering MA Reply Supplementary Analysis - EL-HTP, LE-HTP, and LFE-HTP global reproducibility analysis

## Repository Organization

In main repository path is the Rmarkdown to generated all of the knitted formats: .pdf , .html , and .docx

For re-running all code locally- paths in .Rmd must be changed according to where you place all of the input data folders (below folders Part1 through Part4). Also, nCores in TPP curve fit should be adjusted to personal computing capacity (in Part#3). Lastly, all packages used in analysis must be installed locally (located at top of Rmd). 

In the main branch, there are the following 5 folders:

1. Part#1: Input supplementary data from Huang _et al._ (Nat. Methods 2019) publication for general reproducibility.
2. Part#2: MS database search and quantification results from the Huang _et al._ (Nat. Methods 2019) publication's  RAW files for assessing HTP data reliability. TPP curve fit results also deposited in this folder. 
3. Part#3: Dali input files from MaxQuant database search for all Dali analysis. Analysis includes general method reproducibility, reliablity, statistical tests, and examples of phosphosites that alter protein relative stability (Rs).
4. Part#4: Re-analysis of Moellering group MA reply data to our manuscript which includes new data testing the impact of TMT ratio distortion and TMT labeling/Phospho-Enrichment order on reliability of Tm readouts. Contains the supplementary data from EL-HTP, LE-HTP, and LFE-HTP implementations to assess reproducibility and reliablity across modified HTP methods.
5. Figures: Includes all generated figures and datsets from data analysis in R (via the .Rmd). Additional files included are the datasets formated to journal's publication specifications.
